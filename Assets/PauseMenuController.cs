using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour
{
    [SerializeField] public GameObject spotlight;
    [SerializeField] public Camera pauseMenuCamera;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePosition = Input.mousePosition;
        Vector3 worldPosition = pauseMenuCamera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, -pauseMenuCamera.transform.position.z*1.05f));
        worldPosition.z = 0; // Ensure the z position is 0 to stay in 2D plane
        spotlight.transform.position = worldPosition;
        if (Input.GetKeyDown(KeyCode.Escape)) SceneManager.LoadScene("MenuScene");
    }
}
