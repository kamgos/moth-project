using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreenController : MonoBehaviour
{
    [SerializeField] public Slider progressSlider;
    
    public float currentValue = 0.0f;
    [SerializeField] public float loadingTime = 4.0f;

    // Start is called before the first frame update
    void Start()
    {
        progressSlider.minValue = 0;
        progressSlider.maxValue = loadingTime;
    }

    // Update is called once per frame
    void Update()
    {
        currentValue += Time.deltaTime;
        
        if (currentValue >= loadingTime)
        {
            // SceneManager.LoadScene("Level_1");
            SceneManager.LoadScene("Tutorial");
        }
        progressSlider.value = currentValue;
        
    }
}
