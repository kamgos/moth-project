using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class PauseMenuMothController : MonoBehaviour
{

    [SerializeField] private float horizontalSpeed = 0.05f;
    [SerializeField] private float verticalSpeed = 0.05f;
    [SerializeField] public Light2D lampInSight;

    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 deltaMove = new Vector3();

        float xDistanceToLamp = (lampInSight.transform.position.x - rb.transform.position.x);
        float yDistanceToLamp = (lampInSight.transform.position.y - rb.transform.position.y)-12;

        deltaMove.Set(xDistanceToLamp * horizontalSpeed, yDistanceToLamp * verticalSpeed, 0);

        rb.transform.position = rb.transform.position + deltaMove * Time.unscaledDeltaTime;

    }
}
