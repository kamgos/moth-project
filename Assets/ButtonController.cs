using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    private bool isColliding = false;
    private float timeToClick = 2.0f; // 1 sekunda
    private float currentTime = 0.0f;
    private float originalLightSize;

    [SerializeField] public Light2D spotLight = null;

    // Start is called before the first frame update
    void Start()
    {
        originalLightSize = spotLight.pointLightOuterRadius;
    }

    // Update is called once per frame
    void Update()
    {
        if (isColliding)
        {
            currentTime += Time.deltaTime;
            spotLight.pointLightOuterRadius = originalLightSize * (1+currentTime*1.4f);
        }
        if (currentTime > timeToClick)
        {
            
            switch (gameObject.tag)
            {
                case "StartButton":
                    SceneManager.LoadScene("LoadingScene");
                    break;
                
                case "CreditsButton":
                    SceneManager.LoadScene("CreditsScene");
                    break;

                case "ExitButton":
                    #if UNITY_EDITOR
                    UnityEditor.EditorApplication.isPlaying = false;
                    #else
                    Application.Quit();
                    #endif
                    break;
            }
            
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision != null)
        {
            if(collision.CompareTag("Player")) isColliding = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {        
        if(collision != null)
        {
            if (collision.CompareTag("Player"))
            {
                isColliding = false;
                currentTime = 0.0f;
                spotLight.pointLightOuterRadius = originalLightSize;
            }
        }
    }
}
