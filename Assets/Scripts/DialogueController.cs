using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueController : MonoBehaviour
{
   public GameObject navmesh;
    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "First Dialogue")
        {
            StartCoroutine(FirstDialogue());
        }
    }
   
    IEnumerator FirstDialogue()
    {
        
        navmesh.SetActive(false);
        yield return new WaitForSeconds(10);
    }

    
}
