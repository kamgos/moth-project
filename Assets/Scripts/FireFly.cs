using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireFly : MonoBehaviour
{
    public float Speed = 5f;
    public float verticalSpeed = 7f;
    public float fallingSpeed = 4f;
    
    public GameObject startingPoint;
    public GameObject point1;
    public bool moveToPoint1 { get;  set; }
    // Start is called before the first frame update
    void Start()
    {
        moveToPoint1 = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(moveToPoint1)
        {
            transform.position = Vector3.MoveTowards(transform.position, point1.transform.position, Speed);
        }
    }
}
