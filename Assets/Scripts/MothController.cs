

using UnityEngine;
using System.Collections.Generic;
using System.Runtime.ConstrainedExecution;
using System.Net.Sockets;
using Unity.Mathematics;
using UnityEngine.UI;
using System.Collections;
using LottiePlugin.UI;
using UnityEngine.AI;
using TMPro;
using UnityEditor;
using UnityEngine.SceneManagement;

public class MothController : MonoBehaviour
{
    //moth
    [SerializeField] private float horizontalSpeed = 5f;
    [SerializeField] private float verticalSpeed = 7f;
    [SerializeField] private float fallingSpeed = 4f;
    [SerializeField] private float boostedSpeed = 5f;


    private bool canMoveRight = true;
    private bool attractToLamp = false;
    private bool shielding = false;
    private Rigidbody2D rb;
    public HealthSys hp;
    public int nectarCount;
    public int nectarToIncreaseSpeed = 3;
    public GameObject basicCanvas;
    public GameObject navmesh;


    public GameObject nectar0;
    public GameObject nectar1;
    public GameObject nectar2;
    public GameObject nectar3;
    public GameObject nectar4;

    public GameObject health1;
    public GameObject health2;
    public GameObject health3;
    //moth

    //dialogue
    public int step;
    public TMP_Text dialogueText;
    [TextArea]
    public string[] dialogue;
    public GameObject dialogueCanvas;
    
    //dialogue
    GameObject lampInSight = null;
    
    //npc
    public GameObject Firefly;
    public GameObject Enemy;
    public GameObject point1;
    public GameObject point2;
    public GameObject point3;
    public GameObject point4;
    public float fireflySpeed;
    public bool moveToPoint1=false;
    public bool moveToPoint2=false;
    public bool moveToPoint3 = false;
    public bool moveToPoint4 = false;
    //npc

    
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        hp= GetComponent<HealthSys>();
        
        
    }

    private void Start()
    {
        if(dialogueCanvas) dialogueCanvas.SetActive(false);
    }

    private void Update()
    {
        NectarChecking();
        HealthChecking();
    }

    private void FixedUpdate()
    {
        Vector3 deltaMove = new Vector3();
        if (canMoveRight)
        {
            deltaMove.Set(horizontalSpeed, -fallingSpeed, 0);
        }
        if (attractToLamp && !shielding)
        {
            float xDistanceToLamp = (lampInSight.transform.position.x - rb.transform.position.x) / 5;
            float yDistanceToLamp = (lampInSight.transform.position.y - rb.transform.position.y) / 8;

            deltaMove.Set(xDistanceToLamp * horizontalSpeed, yDistanceToLamp * verticalSpeed, 0);
        }
        
        rb.MovePosition(rb.transform.position + deltaMove * Time.deltaTime);
        if (moveToPoint1)
        {
            Firefly.transform.position = Vector3.MoveTowards(Firefly.transform.position, point1.transform.position, fireflySpeed);
        }
        if(moveToPoint2 && !moveToPoint1) 
        {
            Firefly.transform.position = Vector3.MoveTowards(Firefly.transform.position, point2.transform.position, fireflySpeed);
        }
        if (moveToPoint3 && !moveToPoint1 && !moveToPoint2)
        {
            Firefly.transform.position = Vector3.MoveTowards(Firefly.transform.position, point3.transform.position, fireflySpeed);
        }
        if (moveToPoint4 && !moveToPoint3 && !moveToPoint1 && !moveToPoint2)
        {
            Firefly.transform.position = Vector3.MoveTowards(Firefly.transform.position, point4.transform.position, fireflySpeed);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Nectar"))
        {
            NectarCollect();
        }
        if (other.gameObject.CompareTag("Lamp") && !shielding)
        {
            attractToLamp = true;
            lampInSight = other.gameObject;

            canMoveRight = false;            
        }
        else if (other.gameObject.CompareTag("ShieldingObject"))
        {
            shielding = true;
            canMoveRight = true;
        }
        if (other.tag == "First Dialogue")
        {
            basicCanvas.SetActive(false);
            dialogueCanvas.SetActive(true);
            StartCoroutine(FirstDialogue());
        }
        if (other.tag== "Second Dialogue")
        {
            StartCoroutine(SecondDialogue());
            Firefly.transform.Rotate(0, 180, 0);
            basicCanvas.SetActive(false);
            dialogueCanvas.SetActive(true);
        }
        if (other.tag == "Third Dialogue")
        {
            StartCoroutine(ThirdDialogue());
            Firefly.transform.Rotate(0, 180, 0);
            basicCanvas.SetActive(false);
            dialogueCanvas.SetActive(true);
        }
        if (other.tag == "Fourth Dialogue")
        {
            Enemy.SetActive(false);
            StartCoroutine(FourthDialogue());
            Firefly.transform.Rotate(0, 180, 0);
            basicCanvas.SetActive(false);
            dialogueCanvas.SetActive(true);
        }
        if (other.tag == "End")
        {
            SceneManager.LoadScene("Level_1");
        }

    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Lamp"))
        {
            attractToLamp = false;
            canMoveRight = true;
            lampInSight = null;
        }
        else if (other.gameObject.CompareTag("ShieldingObject")) 
        {
            shielding = false;
        }
    }

    IEnumerator FirstDialogue()
    {

        dialogueText.text = dialogue[step];
        step += 1;
        horizontalSpeed = 0f;
        verticalSpeed = 0f;
        fallingSpeed = 0f;
        yield return new WaitForSeconds(4);
        dialogueText.text = dialogue[step];
        step += 1;
        yield return new WaitForSeconds(4);
        dialogueText.text = dialogue[step];
        step += 1;
        yield return new WaitForSeconds(5);
        dialogueText.text = dialogue[step];
        step += 1;
        yield return new WaitForSeconds(7);
        dialogueText.text = dialogue[step];
        step += 1;
        yield return new WaitForSeconds(7);
        dialogueText.text = dialogue[step];
        step += 1;
        Firefly1();
        yield return new WaitForSeconds(3);
        dialogueCanvas.SetActive(false);

        yield return new WaitForSeconds(1);
        basicCanvas.SetActive(true);
        
        horizontalSpeed = 5f;
        verticalSpeed = 7f;
        fallingSpeed = 4f;
        
    }
    IEnumerator SecondDialogue()
    {
        dialogueText.text = dialogue[step];
        step += 1;
        horizontalSpeed = 0f;
        verticalSpeed = 0f;
        fallingSpeed = 0f;
        yield return new WaitForSeconds(5);
        dialogueText.text = dialogue[step];
        step += 1;
        yield return new WaitForSeconds(5);
        dialogueText.text = dialogue[step];
        step += 1;
        yield return new WaitForSeconds(1);
        Firefly2();
        yield return new WaitForSeconds(3);
        dialogueCanvas.SetActive(false);

        yield return new WaitForSeconds(1);
        basicCanvas.SetActive(true);

        horizontalSpeed = 5f;
        verticalSpeed = 7f;
        fallingSpeed = 4f;
    }
    IEnumerator ThirdDialogue()
    {
        dialogueText.text = dialogue[step];
        step += 1;
        horizontalSpeed = 0f;
        verticalSpeed = 0f;
        fallingSpeed = 0f;
        yield return new WaitForSeconds(5);
        dialogueText.text = dialogue[step];
        step += 1;
        yield return new WaitForSeconds(4);
        dialogueText.text = dialogue[step];
        step += 1;
        yield return new WaitForSeconds(1);
        Firefly3();
        Enemy.SetActive(true); 
        yield return new WaitForSeconds(2);
        dialogueCanvas.SetActive(false);

        yield return new WaitForSeconds(1);
        basicCanvas.SetActive(true);
        if (nectarCount >= 3)
        {
            horizontalSpeed += boostedSpeed;
        }
        else
        {
            horizontalSpeed = 5f;
        }
        verticalSpeed = 7f;
        fallingSpeed = 4f;

    }
    IEnumerator FourthDialogue()
    {
        dialogueText.text = dialogue[step];
        step += 1;
        horizontalSpeed = 0f;
        verticalSpeed = 0f;
        fallingSpeed = 0f;
        yield return new WaitForSeconds(4);
        dialogueText.text = dialogue[step];
        step += 1;
        yield return new WaitForSeconds(5);
        Firefly4();
        yield return new WaitForSeconds(2);
        dialogueCanvas.SetActive(false);

        yield return new WaitForSeconds(1);
        basicCanvas.SetActive(true);
        if(nectarCount>=3)
        {
            horizontalSpeed += boostedSpeed;
        }
        else 
        {
            horizontalSpeed = 5f;
        }
        
        verticalSpeed = 7f;
        fallingSpeed = 4f;
    }

    private void NectarCollect()
    {
        nectarCount++;
        if(nectarCount>=nectarToIncreaseSpeed)
        {
            NectarBoost();
            nectarCount = 0;
        }
    }

    private void NectarChecking()
    {
        if(nectarCount==0)
        {
            Nectar0();
        }
        if (nectarCount == 1)
        {
            Nectar1();
        }
        if (nectarCount == 2)
        {
            Nectar2();
        }
        if (nectarCount == 3)
        {
            Nectar3();
        }
        if (nectarCount == 4)
        {
            Nectar4();
        }
    }

    private void NectarBoost()
    {
        if(nectarCount >= 3)
        {
            horizontalSpeed += boostedSpeed;
        }
    }

    public void Nectar0()
    {
        nectar0.SetActive(true);
        nectar1.SetActive(false);
        nectar2.SetActive(false);
        nectar3.SetActive(false);
        nectar4.SetActive(false);
    }

    public void Nectar1()
    {
        
        nectar0.SetActive(false);
        nectar1.SetActive(true);
        nectar2.SetActive(false);
        nectar3.SetActive(false);
        nectar4.SetActive(false);
    }

    public void Nectar2()
    {
        nectar0.SetActive(false);
        nectar1.SetActive(false);
        nectar2.SetActive(true);
        nectar3.SetActive(false);
        nectar4.SetActive(false);
    }

    public void Nectar3()
    {
        nectar0.SetActive(false);
        nectar1.SetActive(false);
        nectar2.SetActive(false);
        nectar3.SetActive(true);
        nectar4.SetActive(false);
    }
    public void Nectar4()
    {
        nectar0.SetActive(false);
        nectar1.SetActive(false);
        nectar2.SetActive(false);
        nectar3.SetActive(false);
        nectar4.SetActive(true);
    }


    public void HealthChecking()
    {
        if(hp.currentHealth== 3)
        {
            health1.SetActive(true);
            health2.SetActive(false);
            health3.SetActive(false);
        }
        if (hp.currentHealth == 2)
        {
            health1.SetActive(false);
            health2.SetActive(true);
            health3.SetActive(false);
        }
        if (hp.currentHealth ==1)
        {
            health1.SetActive(false);
            health2.SetActive(false);
            health3.SetActive(true);
        }
        if (hp.currentHealth <= 0)
        {
            health1.SetActive(false);
            health2.SetActive(false);
            health3.SetActive(false);
        }
    }
    public void Firefly1()
    {
        Firefly.transform.Rotate(0, -180, 0);
        moveToPoint1 = true;
    }
    public void Firefly2()
    {
        Firefly.transform.Rotate(0, -180, 0);
        moveToPoint2 = true;
        moveToPoint1 = false;
    }
    public void Firefly3()
    {
        Firefly.transform.Rotate(0, -180, 0);
        moveToPoint2 = false;
        moveToPoint1 = false;
        moveToPoint3 = true;
    }
    public void Firefly4()
    {
        Firefly.transform.Rotate(0, -180, 0);
        moveToPoint2 = false;
        moveToPoint1 = false;
        moveToPoint3 = false;
        moveToPoint4 = true;
    }
}


