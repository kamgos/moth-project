using UnityEngine;

public class BackgroundFollow : MonoBehaviour
{
    // Referencja do obiektu, za kt�rym t�o ma pod��a� (np. posta� gracza)
    public Transform target;
    

    // Offset mi�dzy postaci� a t�em w osi X
    private float offsetX;

    void Start()
    {
        // Obliczenie pocz�tkowego offsetu mi�dzy t�em a postaci� w osi X
        offsetX = transform.position.x - target.position.x;
    }

    public float smoothSpeed = 0.125f;

    void LateUpdate()
    {
        float desiredPositionX = target.position.x + offsetX;
        float smoothedPositionX = Mathf.Lerp(transform.position.x, desiredPositionX, Time.deltaTime * smoothSpeed);
        transform.position = new Vector3(smoothedPositionX, transform.position.y, transform.position.z);
    }
}
